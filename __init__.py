# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.pool import Pool
from . import equipment
from . import stock
from . import work


def register():
    Pool.register(
        equipment.EquipmentSystem,
        equipment.EquipmentSystemType,
        stock.Lot,
        work.Work,
        module='electrans_equipment_system', type_='model')
