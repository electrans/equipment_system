# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta
from trytond.pyson import Eval, Bool


class Project(metaclass=PoolMeta):
    __name__ = 'work.project'

    equipment_system = fields.Many2One(
        'equipment.system', "Equipment System",
        states={
            'required': Eval('type') == 'maintenance'},
        depends=['type'])
