# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pyson import Eval, Bool, Not


class EquipmentSystem(ModelSQL, ModelView):
    "Equipment System"
    __name__ = 'equipment.system'

    name = fields.Char("Name")
    parent = fields.Many2One('equipment.system', "Parent")
    customer = fields.Many2One(
        'party.party', "Customer",
        states={
            'required': Not(Bool(Eval('parent'))),
            'readonly': Bool(Eval('parent'))})
    children = fields.One2Many('equipment.system', 'parent', "Children")
    type = fields.Selection([
        ('installation', 'Installation'),
        ('system', 'System'),
        ], 'Type')
    system_type = fields.Many2One(
        'equipment.system.type', "System Type",
        states={
            'invisible': Eval('type') != "system"})
    equipments = fields.One2Many(
        'stock.lot', 'equipment_system', 'Equipments',
        states={
            'invisible': Eval('type') != 'system'})

    @classmethod
    def view_attributes(cls):
        return super(EquipmentSystem, cls).view_attributes() + [
            ('//page[@id="equipments"]', 'states', {
                    'invisible': Eval('type') != 'system'})]

    @fields.depends('parent')
    def on_change_with_customer(self):
        return self.parent.customer.id if self.parent else None

    @fields.depends('type')
    def on_change_with_system_type(self):
        if self.type != 'system':
            return None


class EquipmentSystemType(ModelSQL, ModelView):
    "Equipment System Type"
    __name__ = 'equipment.system.type'

    name = fields.Char("Name")
    preventive_maintenance_document = fields.Binary("Document", filename='name')
