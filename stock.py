# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import ModelView, ModelSQL, fields
from trytond.pool import PoolMeta


class Lot(metaclass=PoolMeta):
    __name__ = 'stock.lot'

    equipment_system = fields.Many2One('equipment.system', "Equipment system")
