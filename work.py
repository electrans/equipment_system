# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelView
from trytond.pool import PoolMeta, Pool


class Work(metaclass=PoolMeta):
    __name__ = 'project.work'

    equipment_system = fields.Many2One('equipment.system', "Equipment System")
